include config/vars

BOOK_LIST := core stories judgement

$(DBOOK): images/ $(BOOK_LIST)
	@$(COMPILER) main.tex

config/vars:
	@git submodule update --init

images/:
	cp -r core/images .
	cp -r stories/images .
	cp -r judgement/images .

##### Standalone Books #####
standalone/:
	mkdir -p standalone

STANDALONE_LIST := $(patsubst %,standalone/%.pdf,$(BOOK_LIST))
.PHONY: standalone-books
standalone-books: $(STANDALONE_LIST)

%/standalone.pdf: %
	make -C $< -e TITLE=standalone -e DROSS=$(LOCAL_DROSS)

standalone/%.pdf: % standalone/ %/standalone.pdf
	@$(CP) $</standalone.pdf $@

##### Complete Books #####

COMPLETE_LIST := complete/Core_Rules.pdf complete/Stories.pdf complete/Judgement.pdf complete/shield.pdf

$(COMPLETE_LIST): complete/

.PHONY: complete-books
complete-books: $(COMPLETE_LIST)

targets += $(STANDALONE_LIST) $(COMPLETE_LIST)

complete/:
	mkdir -p complete

$(LOCAL_DROSS)/core.glg:
	make -C core all

$(LOCAL_DROSS)/stories.glg: $(LOCAL_DROSS)/core.glg
	make -C stories all

$(LOCAL_DROSS)/judgement.glg: $(LOCAL_DROSS)/stories.glg
	make -C judgement -j 2 all

complete/Core_Rules.pdf: $(LOCAL_DROSS)/judgement.glg
	make -C core all
	$(CP) $(LOCAL_DROSS)/core.pdf $@

complete/Stories.pdf: complete/Core_Rules.pdf
	make -C stories all
	$(CP) $(LOCAL_DROSS)/stories.pdf $@

complete/Judgement.pdf: complete/Stories.pdf
	make -C judgement all
	$(CP) $(LOCAL_DROSS)/judgement.pdf $@
complete/shield.pdf: complete/Judgement.pdf
	$(CP) $(LOCAL_DROSS)/shield.pdf $@

##### Cleanup #####

output += standalone complete

.PHONY: clean-books
clean-books:
	make clean -C core
	make clean -C core/config
	make clean -C stories
	make clean -C stories/config
	make clean -C judgement
	make clean -C judgement/config

