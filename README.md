# Metabind

[Download][metabind]

Meta-BIND is a project combining the various BIND tabletop RPG books.

# Dependencies

- `git-lfs`
- `inkscape`
- `make`
- Lots of LaTeX.

# Meta-BIND

This compiles the complete BIND rules, together with everything on Fenestra, providing a complete rulebook for the GM:

```bash
make
```

It also compiles a complete set of the core trio, so that each book can refer to the others by page-number:

```bash
make complete-books
```

The books can also be compiled without the  cross-referencing, just to check they compile okay individually:

```bash
make standalone-books
```

And, of course, you can compile everything with:

```bash
make all
```

[issues]: mailto:contact-project+bindrpg-metabind-37742073-issue-@incoming.gitlab.com
[metabind]: https://gitlab.com/bindrpg/metabind/-/jobs/artifacts/master/raw/Metabind.pdf?job=build
[core]: https://gitlab.com/bindrpg/metabind/-/jobs/artifacts/master/raw/complete/Core_Rules.pdf?job=build
[stories]: https://gitlab.com/bindrpg/metabind/-/jobs/artifacts/master/raw/complete/Stories.pdf?job=build
[judgement]: https://gitlab.com/bindrpg/metabind/-/jobs/artifacts/master/raw/complete/Judgement.pdf?job=build
[shield]: https://gitlab.com/bindrpg/metabind/-/jobs/artifacts/master/raw/complete/shield.pdf?job=build
